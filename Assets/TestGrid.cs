﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGrid : MonoBehaviour
{
    public GameObject test;
    private Vector2 movement;
    private Camera c;
    private Grid<bool> grid;
    // Start is called before the first frame update
    void Start()
    {
        grid = new Grid<bool>(2, 2, 1, (Grid<bool> g, int x, int y) => new bool());
        c = Camera.main;
    }

    private void Update()
    {
        if (PlayerController.Instance.leftMouse)
        {
            movement = PlayerController.Instance.mouse;
            var i = c.ScreenToWorldPoint(new Vector3(movement.x, movement.y, 5));
            grid.SetValue(i, true);
            print("Value at the coordinates of: " + i + " changed to:");
            print("                      -   " + grid.GetValue(i));
            PlayerController.Instance.leftMouse = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toolbar : MonoBehaviour
{
    public static Toolbar Instance;

    public Image selector;
    // TODO: Set manually!
    public GameObject[] toolbarSlots;
    int i;
    public float sensitivity = 15f;
    public Item item;
    //public Tool tool;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //int input = (int)(-Input.mouseScrollDelta.y / 100 * sensitivity);
        //if (input != 0)
        //{
        //    i += input;
        //    if (i > toolbarSlots.Length - 1)
        //    {
        //        i = 0;
        //    }
        //    else if (i < 0)
        //    {
        //        i = toolbarSlots.Length - 1;
        //    }            
        //}

        selector.transform.position = toolbarSlots[i].transform.position;
        item = toolbarSlots[i].GetComponent<InventorySlotScript>().Item;
    }
}

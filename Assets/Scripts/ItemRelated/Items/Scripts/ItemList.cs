﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemList : MonoBehaviour
{
    public List<Item> itemIDList = new List<Item>();
    public List<string> itemNameList = new List<string>();

    public GameObject itemPrefab;

    public static ItemList instance;

    private void Awake()
    {
        instance = this;

        for (int i = 0; i < itemIDList.Count; i++)
        {
            itemNameList.Add(itemIDList[i].Name.ToLower());
            print(itemNameList[i] + " -> was added to ItemNameList");
        }
    }
    public Item ItemName(string name)
    {
        for (int i = 0; i < itemNameList.Count; i++)
        {
            if (itemNameList[i] == name)
            {
                return itemIDList[i];
            }

        }

        Debug.LogWarning("No items found!");
        return null;
    }

    public Item ItemID(string id)
    {
        foreach (var i in itemIDList)
        {
            if (i.Name == id)
            {
                return i;
            }
        }
        return null;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 t;
    private Transform child;
    
    // Start is called before the first frame update
    private void Start()
    {
        //child = GetComponentInChildren<Transform>();
        child = GetComponentInParent<Transform>();
        t = child.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        child.position = Input.mousePosition;
    }
    
    public void OnEndDrag(PointerEventData eventData)
    {
        child.position = t;
    }
}

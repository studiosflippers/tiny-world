﻿using UnityEngine;
public enum Tool
{
    ITEM,
    AXE,
    PICKAXE,
    SWORD,
    BOW,
    STAFF
}

public enum Material
{
    VOID,
    WOOD,
    STONE,
    IRON,
    COPPER,
    GOLD,
    SILVER,
    TITANIUM
}

[CreateAssetMenu(fileName = "new_item", menuName = "item")]
public class Item : ScriptableObject, IMoveable
{

    new public string name = "New Item";
    public string Name
    {
        get
        {
            return name;
        }
    }

    public Sprite icon;
    public Sprite Icon
    {
        get
        {
            return icon;
        }
    }

    public int stackSize;
    public int StackSize
    {
        get
        {
            return stackSize;
        }
    }

    public int durability;
    public int Durability
    {
        get
        {
            return durability;
        }
        set
        {
            durability = value;
        }
    }

    public bool isTool;
    public bool IsTool
    {
        get
        {
            return isTool;
        }
    }

    public bool isBreakable; 
    public bool IsBreakable
    {
        get
        {
            return isBreakable;
        }
    }

    public Tool tool;
    public Tool Tool
    {
        get
        {
            return tool;
        }
    }

    public Material material;
    public Material Material
    {
        get
        {
            return material;
        }
    }

    private InventorySlotScript slot;
    public InventorySlotScript Slot
    {
        get
        {
            return slot;
        }
        set
        {
            slot = value;
        }
    }

    // Called when the item is pressed in the inventory
    public virtual void Use()
    {
        RemoveFromInventory(Slot);
        Debug.Log("Using " + name);
    }

    public void RemoveFromInventory(InventorySlotScript _slot)
    {
        if (_slot != null)
        {
            _slot.RemoveItem(this);
        }
    }

}
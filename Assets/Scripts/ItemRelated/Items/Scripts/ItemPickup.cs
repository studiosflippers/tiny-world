﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickup : Interactable
{
    public Item item;
    public SpriteRenderer icon;

    private void Start()
    {
        icon = GetComponent<SpriteRenderer>();
        icon.sprite = item.icon;
    }
    public override void Interact()
    {
        base.Interact();
        PickUp();
    }

    void PickUp()
    {
        bool wasPickedUp = InventoryScript.instance.AddItem(item);
        if (wasPickedUp)
        {
            Destroy(gameObject);
        }

    }
}

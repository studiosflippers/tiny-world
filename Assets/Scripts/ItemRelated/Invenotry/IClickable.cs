﻿using System;
using UnityEngine.UI;

public interface IClickable
{
    Image Image
    {
        get;
        set;
    }

    int Count
    {
        get;
    }
}

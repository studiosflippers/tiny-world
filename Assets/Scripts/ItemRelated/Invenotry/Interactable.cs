﻿using System;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    private Rigidbody2D rb;
    private const int Speed = 1;
    private Transform player;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //rb = GetComponent<Rigidbody2D>();
    }

    //You can override this method
    public virtual void Interact()
    {

    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        
        if (Vector2.Distance(player.position, transform.position) <= radius)
        {
            Interact();
        }
    }
}
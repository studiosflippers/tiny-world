﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour
{
    public static InventoryScript instance;
    private void Awake()
    {
        instance = this;
    }

    // TODO: Set manually!
    public GameObject[] inventorySlots;
    public List<InventorySlotScript> slots = new List<InventorySlotScript>();

    private InventorySlotScript fromSlot;
    public InventorySlotScript FromSlot
    {
        get
        {
            return fromSlot;
        }
        set
        {
            fromSlot = value;
            if (value != null)
            {
                fromSlot.icon.color = Color.grey;
            }
        }
    }

    private void Start()
    {
        foreach (var slot in inventorySlots)
        {
            var s = slot.GetComponent<InventorySlotScript>();
            slots.Add(s);
        }
    }

    public bool AddItem(Item item)
    {
        if (item.StackSize > 0)
        {
            if (PlaceInStack(item))
            {
                return true;
            }
        }
        if (PlaceInEmpty(item)) return true;
        return false;
    }

    private bool PlaceInEmpty(Item item)
    {
        foreach (var i in slots)
        {
            if (i.AddItem(item))
            {
                return true;
            }
        }

        return false;
    }

    private bool PlaceInStack(Item item)
    {
        foreach (var s in slots)
        {
            if (s.StackItem(item))
            {
                return true;
            }
        }
        return false;
    }
}

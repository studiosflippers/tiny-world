﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlotScript : MonoBehaviour, IPointerClickHandler
{
    private ObservableStack<Item> items = new ObservableStack<Item>();
    public Image icon;
    public Text text;

    private void Awake()
    {
        //items.OnPop += Hi;
    }

    void Hi()
    {
        print("Hi!");
    }

    public bool IsEmpty
    {
        get { return items.Count == 0; }
    }

    public bool IsFull
    {
        get
        {
            if (IsEmpty || Count < Item.StackSize)
            {
                return false;
            }
            return true;
        }
    }

    public Item Item
    {
        get
        {
            if (!IsEmpty)
            {
                return items.Peek();
            }

            return null;
        }
    }


    public int Count
    {
        get { return items.Count; }
    }

    public bool AddItem(Item item)
    {
        if (IsEmpty)
        {
            items.Push(item);
            icon.sprite = item.Icon;
            icon.enabled = true;
            text.text = items.Count.ToString();
            item.Slot = this;
            icon.color = Color.white;
            return true;
        }
        if (StackItem(item)) return true;
        return false;
    }

    public bool AddItems(ObservableStack<Item> newItems)
    {
        if (IsEmpty || newItems.Peek().GetType() == Item.GetType())
        {
            var count = newItems.Count;
            //print(count);
            for (int i = 0; i < count; i++)
            {
                if (IsFull)
                {
                    return false;
                }

                AddItem(newItems.Pop());
            }
            return true;
        }
        return false;
    }

    public void RemoveItem(Item item)
    {
        if (!IsEmpty)
        {
            items.Pop();
            text.text = items.Count.ToString();
            if (IsEmpty)
            {
                icon.sprite = null;
                icon.enabled = false;
                text.text = null;
                item.Slot = null;
            }
        }
    }

    public bool StackItem(Item item)
    {
        if (!IsEmpty && item.name == Item.name && items.Count < Item.StackSize)
        {
            items.Push(item);
            text.text = items.Count.ToString();
            item.Slot = this;
            return true;
        }

        return false;

    }

    private bool PutItemBack()
    {
        if (InventoryScript.instance.FromSlot == this)
        {
            InventoryScript.instance.FromSlot.icon.color = Color.white;
            return true;
        }
        return false;
    }

    bool SwapItems(InventorySlotScript from)
    {
        if (IsEmpty) return false;

        // if moving item is different or if cannot be added to stack
        if (from.Item.name != Item.name || from.Count + Count > Item.StackSize)
        {
            //copy of all the items
            ObservableStack<Item> tmpFrom = new ObservableStack<Item>(from.items);

            //clear a
            from.items.Clear();
            //from b to a
            from.AddItems(items);
            from.icon.color = Color.white;

            //b clear
            items.Clear();
            //from a to b
            AddItems(tmpFrom);
            return true;
        }

        return false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {

            if (InventoryScript.instance.FromSlot == null && !IsEmpty)
            {
                HandScript.instance.TakeMoveable(Item as IMoveable);
                InventoryScript.instance.FromSlot = this;
            }

            else if (InventoryScript.instance.FromSlot != null)
            {
                if (PutItemBack() || SwapItems(InventoryScript.instance.FromSlot) || AddItems(InventoryScript.instance.FromSlot.items))
                {
                    HandScript.instance.Drop();
                    if (InventoryScript.instance.FromSlot.IsEmpty)
                    {
                        InventoryScript.instance.FromSlot.icon.color = Color.white;
                        InventoryScript.instance.FromSlot.icon.enabled = false;
                        InventoryScript.instance.FromSlot.text.text = null;
                    }
                    InventoryScript.instance.FromSlot = null;
                }
            }

        }
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            print("using");
            RemoveItem(Item);
        }
    }
}

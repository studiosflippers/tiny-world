﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour
{
    public static UserInterface Instance;

    PlayerController pc;
    bool inventoryInput;
    bool chatInput;
    bool cancelInput;
    bool changableVariable;


    [HideInInspector] public int weight = 0;

    public GameObject inventoryUI; //w=1
    public GameObject chat;        //w=1
    //public GameObject pauseMenu;//w=-1
    //public GameObject settings;//w=-2
    //public GameObject controls;//w=-3
    //public GameObject inputSelector;//w=-4
    //public GameObject sounds;//w=-2
    //public GameObject graphics;//w=-2

    //public InventorySlot[] slots;
    //private InventoryScript inventory;

    public void SetWeight(int w)
    {
        weight = w;
    }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        pc = PlayerController.Instance;
        chat.SetActive(false);
        changableVariable = true;
    }

    private void Update()
    {
        inventoryInput = pc.inventory;
        chatInput = pc.chat;
        cancelInput = pc.cancel;


        if (cancelInput && changableVariable)
        {
            Weight();
            print(weight);
            changableVariable = false;
        }
        if (!cancelInput)
        {
            changableVariable = true;
        }

        if (inventoryInput)
        {
            if (weight == 0)
            {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                pc.inventory = false;
                weight = 1;
            }
            else if (inventoryUI.activeInHierarchy)
            {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                pc.inventory = false;
                weight = 0;
            }
        }

        if (chatInput && chat.activeSelf == false && weight == 0)
        {
            chat.SetActive(!chat.activeSelf);
            pc.chat = false;
            weight = 1;
        }


        switch (weight)
        {
            //case 1:
                //inventory, chat...
            case 0:
                //all menus off!
                chat.SetActive(false);
                inventoryUI.SetActive(false);
                break;
            case -1:
                //pauseMenu
                print("pause menu");
                break;
        }
    }

    void Weight()
    {
        if (weight >= 0)
        {
            weight--;
        }
        else if (weight <= -1)
        {
            weight++;
        }
    }
}

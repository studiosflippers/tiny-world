﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandScript : MonoBehaviour
{
    PlayerController pc;
    Vector3 mousePos;

    public IMoveable Moveable { get; set; }

    private Image icon;
    public Vector3 offset;

    public static HandScript instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        pc = PlayerController.Instance;
        icon = GetComponent<Image>();
    }

    private void Update()
    {
        mousePos = pc.mouse;
        icon.transform.position = mousePos + offset;
    }

    public void TakeMoveable(IMoveable moveable)
    {
        this.Moveable = moveable;
        icon.enabled = true;
        icon.sprite = moveable.Icon;
        
    }

    public void Drop()
    {
        Moveable = null;
        icon.enabled = false;
    }
}

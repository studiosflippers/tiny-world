﻿using System;
using UnityEngine;



public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public InputManager manager;
    Vector2 movementInput;
    [HideInInspector] public bool leftMouse;    //
    [HideInInspector] public bool rightMouse;   //
    [HideInInspector] public bool interacting;  //Entrance
    [HideInInspector] public bool inventory;    //
    [HideInInspector] public bool chat;         //UserInterface
    [HideInInspector] public bool cancel;       //
    [HideInInspector] public Vector2 mouse;     //Resources
    [HideInInspector] public bool saveGameButton;     //Resources

    public float speed = 5f;
    private Rigidbody2D rb;
    private Animator anim;
    private Transform childTransform;

    private Vector2 movement;
    private static readonly int Speed = Animator.StringToHash("Speed");


    private UserInterface ui;
    int weight;

    public void Awake()
    {
        Instance = this;

        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        childTransform = GetComponentInChildren<Transform>();

        manager = new InputManager();
        manager.Player.Movement.performed += ctx => movementInput = ctx.ReadValue<Vector2>();
        manager.Player.Interact.performed += ctx => interacting = ctx.ReadValueAsButton();
        manager.Player.Inventory.performed += ctx => inventory = ctx.ReadValueAsButton();
        manager.Player.Chat.performed += ctx => chat = ctx.ReadValueAsButton();
        manager.Player.Cancle.performed += ctx => cancel = ctx.ReadValueAsButton();
        manager.Player.MouseInput.performed += ctx => mouse = ctx.ReadValue<Vector2>();
        manager.Player.LeftMouseButton.performed += ctx => leftMouse = ctx.ReadValueAsButton();
        manager.Player.RightMouseButton.performed += ctx => rightMouse = ctx.ReadValueAsButton();
        manager.Player.SaveGameButton.performed += ctx => saveGameButton = ctx.ReadValueAsButton();

        interacting = false;
        inventory = false;
        chat = false;
        leftMouse = false;
    }

    private void Start()
    {
        ui = UserInterface.Instance;
    }

    // Update is called once per frame
    //private void Update()
    //{
    //    //print(cancle);
    //    //TODO: CHeck this!
    //    weight = ui.weight;
    //    //weight = 0;
    //    if (weight != 0)
    //    {
    //        return;
    //    }
    //    else
    //    {
    //        movement = movementInput;
    //        anim.SetFloat(Speed, movement.sqrMagnitude);
    //    }
    //}



    //Update is called once per fixed time (50x sec)
    private void FixedUpdate()
    {
        //Turning player
        if (Math.Abs(movement.x) > 0.01)
        {
            //Fixing scale if pressed both north and vertical input(0.7)
            float s = movement.x;
            double ss = Math.Round(Convert.ToDouble(s));
            s = Convert.ToSingle(ss);
            childTransform.localScale = new Vector2(s, 1);
        }

        //Movement
        rb.MovePosition(rb.position + movement * (speed * Time.fixedDeltaTime));
    }

    private void OnEnable()
    {
        manager.Enable();
    }

    private void OnDisable()
    {
        manager.Disable();
    }
}
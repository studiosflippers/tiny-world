﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeveloperConsole
{
    private readonly string prefix;
    private readonly IEnumerable<IConsoleCommand> commands;

    public DeveloperConsole(string prefix, IEnumerable<IConsoleCommand> commands)
    {
        this.prefix = prefix;
        this.commands = commands;
    }

    public void ProcessCommand(string commandInput, string[] args)
    {
        foreach (var item in commands)
        {
            if (!commandInput.Equals(item.CommandWord, StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }

            if (item.Process(args))
            {
                return;
            }
        }
    }

    public void ProcessCommand(string inputValue)
    {
        if (!inputValue.StartsWith(prefix))
        {
            return;
        }

        inputValue = inputValue.Remove(0, prefix.Length);

        string[] inputSplit = inputValue.Split(' ');

        string commandInput = inputSplit[0];
        string[] args = inputSplit.Skip(1).ToArray();

        ProcessCommand(commandInput, args);
    }

}

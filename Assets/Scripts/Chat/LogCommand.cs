﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Log Command .", menuName = "Commands/Log Command")]
public class LogCommand : ConsoleCommand
{
    public override bool Process(string[] args)
    {
        string logText = string.Join(" ", args);

        Debug.Log("Logged message: " + logText);

        return true;
    }
}

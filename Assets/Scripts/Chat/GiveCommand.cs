﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Give Command", menuName = "Commands/Give Commmand")]
public class GiveCommand : ConsoleCommand
{
    string itemName;
    string playerArg = "player";

    public override bool Process(string[] args)
    {
        // null     1       2       3       or 012
        //give    player   wood   10(value)
        if (args.Length != 3)
        {
            Debug.Log("Too many arguments");
            return false;
        }

        if (args[0] != playerArg)
        {
            Debug.Log("Player not found!");
            return false;
        }

        foreach (var item in ItemList.instance.itemNameList)
        {
            if (args[1] != item)
            {
                itemName = "fail";
                continue;
            }
            else
            {
                itemName = item;
                Debug.Log("Item found with name: " + itemName);
            }
        }

        if (itemName == "fail")
        {
            return false;
        }

        if (!float.TryParse(args[2], out float value))
        {
            return false;
        }

        for (int i = 0; i < value; i++)
        {
            InventoryScript.instance.AddItem(ItemList.instance.ItemName(itemName));
        }

        itemName = string.Empty;
        Debug.Log("Item added to inventory of a player");

        return true;
    }
}

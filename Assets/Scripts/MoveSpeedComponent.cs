﻿using Unity.Entities;

namespace _Scripts
{
    public struct MoveSpeedComponent : IComponentData
    {
        public float moveSpeed;
    }
}

﻿using UnityEngine;

namespace _Scripts
{
    public class Test : MonoBehaviour
    {
        enum Direction
        {
            North, South, East, West
        }

        int chunkSize = 16;
        int times;

        int[,] chunk;
        int[,] _Map;


        public int testValueX;
        public int testValueY;

        public int minusValueY;

        public bool start;
        public bool length;
        // Start is called before the first frame update
        void Start()
        {
            chunk = new int[chunkSize, chunkSize];
            _Map = chunk;
            start = false;
            length = false;

            for (int x = 0; x < chunkSize; x++)
            {
                for (int y = 0; y < chunkSize; y++)
                {
                    chunk[x, y] = Random.Range(0,2);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (start)
            {
                Debug.Log(chunk[testValueX, testValueY]);
                start = false;
            }

            if (length)
            {
                //chunk[] -= chunkSize, chunkSize - minusValueY;
                Debug.Log(chunk.Length);
                length = false;
            }
        }
    }
}

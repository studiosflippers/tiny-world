﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Ore = RoomGenerator.Ore;

public class Resources : MonoBehaviour
{

    PlayerController pc;
    Vector2 movement;
    bool leftMouse;


    private Camera c;


    private Vector3Int m;
    public Vector3 i;
    public LayerMask allTilesLayer;
    private RaycastHit2D rayHit;

    // Hits needed to destroy object
    private int counter;
    private int rand;

    //Tiles
    public Tile tileBush;
    public Tile tileOreIron;
    private Tilemap tilemap;

    [Header("Items")]
    public GameObject ItemHolder;
    public GameObject Empty;
    public Item stick;
    public Item ironClump;

    //public Tool tool;

    public static Resources Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        pc = PlayerController.Instance;
        counter = 0;
        c = Camera.main;
        tilemap = GetComponent<Tilemap>();
    }

    private void FixedUpdate()
    {
        i = c.ScreenToWorldPoint(new Vector3(movement.x, movement.y, 5));
        m = tilemap.WorldToCell(i);
        rayHit = Physics2D.Raycast(i, Vector2.zero, Mathf.Infinity, allTilesLayer);
    }

    // Update is called once per frame
    private void Update()
    {
        movement = pc.mouse;
        leftMouse = pc.leftMouse;

        

        

        if (!leftMouse) return;
        if (rayHit.collider == null) return;
        var t = tilemap.GetTile(m);
        if (!t) print("Object Doesn't Exist");



        if (t == tileBush)
        {
            // Setting random number of hits!
            if (counter == 0)
            {
                rand = Random.Range(3, 6);
                counter++;
            }

            rand--;
            //print(rand);
            if (rand != 0) return;
            tilemap.SetTile(m, null);
            counter = 0;

            Empty.GetComponent<ItemPickup>().item = stick;
            Instantiate(Empty, m, Quaternion.identity, ItemHolder.transform);

            Ore ore = new Ore { x = m.x, y = m.y };
            RoomGenerator.Instance.oresToRemove.Add(ore);
        }
        else if (t == tileOreIron)
        {
            //if (tool.material == Material.Wood) return;

            tilemap.SetTile(m, null);

            Empty.GetComponent<ItemPickup>().item = ironClump;
            Instantiate(Empty, m, Quaternion.identity, ItemHolder.transform);

            Ore ore = new Ore { x = m.x, y = m.y };
            RoomGenerator.Instance.oresToRemove.Add(ore);
        }
    }
}
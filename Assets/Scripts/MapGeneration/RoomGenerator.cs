﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public enum Direction
{
    Right,
    Left,
    Up,
    Down,
}

public class RoomGenerator : MonoBehaviour
{
    public static RoomGenerator Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #region variables

    PlayerController pc;
    bool saveGameButton;  //only temporary

    public delegate void OnWorldChanged();
    public OnWorldChanged OnWorldChangedCallback;
    //OnWorldChangedCallback?.Invoke();

    private Direction direction;

    private Coord firstCoord;
    private List<Coord> savedCoord;
    private List<Coord> allMapCoords;
    private Coord coord;
    private int numOfPaths;


    [Header("Map Properties")] public int width;
    public int height;
    private int[,] map;
    [Range(0, 10)]
    public int pathThickness;                                                   //Tunnel between two rooms
    [Range(1, 5)]
    public int passageTunnel;                                                   //Tunnel between two caves
    public bool useRandomSeed;
    public string seed;
    [Range(0, 60)]
    public int randomFillPercent;
    public int wallThresholdSize = 50;
    public int roomThresholdSize = 50;
    private int memoryFill;
    public float radius = 128f;
    [Range(1, 4)]
    public float radiusModifier = 1.5f;
    private int tries;
    private List<GameObject> nodeList;
    private GameObject nullable;
    private float savedRadius;
    private bool generating;


    [Header("Tiles")]
    public Tilemap tilemapWall;
    public Tilemap shadowOfWall;
    public RuleTile tileShadow;
    public Tilemap tilemapFloor;
    public Tilemap tilemapOre;
    public Tile tileFloor;
    public RuleTile tileWall;
    public Tile iron;
    public Tile bush;
    public int oreSpawnChance = 1;
    public GameObject player;

    #endregion

    List<SaveObject> saveRooms = new List<SaveObject>();
    List<SaveObject> loadRooms = new List<SaveObject>();
    List<Coord> loadRoomCoords = new List<Coord>();
    List<Direction> supportiveDirections = new List<Direction>();
    public List<Ore> oresList = new List<Ore>();
    public List<Ore> oresToRemove;
    List<Ore> loadOre = new List<Ore>();

    #region saving
    void Load()
    {
        Object saveObject = SaveSystem.LoadRecentObject<Object>();
        if (saveObject == null)
        {
            return;
        }
        foreach (var item in saveObject.tilemapRoom)
        {
            loadRooms.Add(item);
        }
    }

    void Save()
    {
        List<SaveObject> saveRoomArray = new List<SaveObject>();

        foreach (var item in saveRooms.ToList())
        {
            saveRoomArray.Add(item);
        }

        Object saveObject = new Object { tilemapRoom = saveRoomArray.ToArray() };
        SaveSystem.SaveObject(saveObject, false);
    }

    public class Object
    {
        public SaveObject[] tilemapRoom;
    }


    [Serializable]
    public class SaveObject
    {
        public int x;
        public int y;
        public string seed;
        public Direction[] directions;
        public Ore[] ores;
    }
    [Serializable]
    public class Ore
    {
        public int x;
        public int y;
        public string name;
    }
    public SaveObject SaveRoom()
    {
        return new SaveObject
        {
            x = coord.TileX,
            y = coord.TileY,
            seed = seed,
            directions = supportiveDirections.ToArray(),
            ores = oresList.ToArray(),

        };
    }


    #endregion saving

    private void Start()
    {
        pc = PlayerController.Instance;
        //**Modifiers**
        memoryFill = randomFillPercent;                                         //First room will have more space for village
        generating = false;                                                     //Sets radius * radius modifier on true
        radius += 2;                                                            //Adding 2 to radius for making it cover all spawns
        savedRadius = radius;                                                   //Saving default radius (+ 2) because of first generation
        tries = 0;

        //**Starts building the map**
        firstCoord = new Coord(0, 0);                                           //Instantiating Coord 0,0
        savedCoord = new List<Coord>() { firstCoord };                          //Adding first coord to the savedCoord list so it generates first chunk

        nullable = new GameObject("Nullable Holder!");                          //Holder for nodes
        nodeList = new List<GameObject>();
        allMapCoords = new List<Coord>();
        saveRooms = new List<SaveObject>();
        Load();
        foreach (var item in loadRooms.ToList())
        {
            loadRoomCoords.Add(new Coord(item.x, item.y));
        }


    }

    public void Update()
    {
        saveGameButton = pc.saveGameButton;

        if (generating)
        {
            radius = savedRadius * radiusModifier;
        }
        else
        {
            radius = savedRadius;
        }

        foreach (var n in nodeList.ToList())
        {
            if (!(Vector2.Distance(player.transform.position, n.transform.position) <= radius))
            {
                generating = false;
                continue;
            }

            generating = true;
            var position = n.transform.position;                            //*
            var x = (int)(position.x + width / 2f) / width - 1;             //Sets node from center to 0,0 OF ROOM
            var y = (int)(position.y + height / 2f) / height - 1;           //*
            savedCoord.Add(new Coord(x, y));                                //Creates coord to instantiate room(coord pos from up*)
            nodeList.Remove(n);                                             //Removes from node list
            Destroy(n);                                                     //Destroys node.
        }

        foreach (var c in savedCoord.ToList())
        {
            coord = c;
            if (tries == 0)
            {
                randomFillPercent = 40;                                     //First room with bigger space for village
                numOfPaths = 3;                                             //Four paths from first room
            }
            else
            {
                randomFillPercent = memoryFill;                             //fillPercent back to the original
                numOfPaths = RandomNumber(2);                               //Number of paths (1 - 3; ---> 0 - 2 in code)
            }
            var dir = new List<Direction>();                                    //Building List of All Directions to Execute!

            if (loadRoomCoords.Contains(c))                                     //if coord is saved
            {
                useRandomSeed = false;
                foreach (var item in loadRooms)
                {
                    if (c.TileX == item.x)
                    {
                        if (c.TileY == item.y)
                        {
                            seed = item.seed;
                            supportiveDirections = new List<Direction>();       //List of directions ONLY for saving purpose
                            foreach (var d in item.directions)
                            {
                                dir.Add(d);
                                supportiveDirections.Add(d);
                            }
                            loadOre = new List<Ore>();
                            oresList = new List<Ore>();
                            foreach (var o in item.ores)
                            {
                                loadOre.Add(new Ore { x = o.x, y = o.y, name = o.name });
                            }
                        }
                    }
                }

                GenerateMap();
                foreach (var d in dir)
                {
                    direction = d;
                    GenerateNextCoord();
                    ConnectionToPrev();
                }

                //Setting tiles in map
                for (var x = 0; x < width; x++)
                {
                    for (var y = 0; y < height; y++)
                    {
                        var pos = new Vector3Int(x + c.TileX * width, y + c.TileY * height, 0);
                        if (map[x, y] == 1)
                        {
                            tilemapWall.SetTile(pos, tileWall);
                            shadowOfWall.SetTile(pos, tileShadow);
                        }

                        tilemapFloor.SetTile(pos, tileFloor);
                    }
                }
                foreach (var item in loadOre)
                {
                    Vector3Int pos = new Vector3Int(item.x, item.y, 0);
                    if (item.name == iron.name)
                    {
                        tilemapOre.SetTile(pos, iron);
                        oresList.Add(new Ore
                        {
                            x = pos.x,
                            y = pos.y,
                            name = iron.name,
                        });
                    }
                    else if (bush.name == item.name)
                    {
                        tilemapOre.SetTile(pos, bush);
                        oresList.Add(new Ore
                        {
                            x = pos.x,
                            y = pos.y,
                            name = bush.name,
                        });
                    }

                }

                saveRooms.Add(SaveRoom());                                      //Adds all created rooms to save folder so it can save them
                allMapCoords.Add(c);
                savedCoord.Remove(c);
                tries = 1;
            }
            else                                                                //if there is no saved coord
            {
                useRandomSeed = true;
                if (tries == 0)
                {
                    randomFillPercent = 40;                                     //First room with bigger space for village
                    numOfPaths = 3;                                             //Four paths from first room 3 + 1
                }
                else
                {
                    randomFillPercent = memoryFill;                             //fillPercent back to the original
                    numOfPaths = RandomNumber(2);                               //Number of paths (1 - 3; ---> 0 - 2 in code)
                }

                supportiveDirections = new List<Direction>();                   //List of directions ONLY for saving purpose
                var oldDir = ArrayListOfDirections();                           //Building Array of All Available Directions.
                for (var i = 0; i <= numOfPaths; i++)
                {
                    var rand = Random.Range(0, oldDir.Count);
                    dir.Add(oldDir[rand]);
                    supportiveDirections.Add(oldDir[rand]);
                    var direct = oldDir[rand];
                    oldDir.Remove(direct);
                }

                GenerateMap();
                foreach (var d in dir)
                {
                    direction = d;
                    GenerateNextCoord();
                    ConnectionToPrev();
                }

                for (var x = 0; x < width; x++)                                 //SETTING & SAVING ores on to the tilemap
                {
                    for (var y = 0; y < height; y++)
                    {
                        var pos = new Vector3Int(x + c.TileX * width, y + c.TileY * height, 0);
                        if (map[x, y] == 1)
                        {
                            tilemapWall.SetTile(pos, tileWall);
                            shadowOfWall.SetTile(pos, tileShadow);
                        }
                        else
                        {
                            var rand = RandomNumber(1000);
                            if (rand < oreSpawnChance)
                            {
                                var r = RandomNumber(1);
                                switch (r)
                                {
                                    case 0:
                                        tilemapOre.SetTile(pos, iron);
                                        oresList.Add(new Ore
                                        {
                                            x = pos.x,
                                            y = pos.y,
                                            name = iron.name,
                                        });
                                        break;
                                    case 1:
                                        tilemapOre.SetTile(pos, bush);
                                        oresList.Add(new Ore
                                        {
                                            x = pos.x,
                                            y = pos.y,
                                            name = bush.name,
                                        });
                                        break;
                                    default:
                                        tilemapOre.SetTile(pos, iron);
                                        oresList.Add(new Ore
                                        {
                                            x = pos.x,
                                            y = pos.y,
                                            name = iron.name,
                                        });
                                        break;
                                }
                            }
                        }

                        tilemapFloor.SetTile(pos, tileFloor);
                    }
                }

                saveRooms.Add(SaveRoom());                                      //Adds all created rooms to save folder so it can save them
                allMapCoords.Add(c);                                            //Helps with the directions(so the maps don't stack)
                savedCoord.Remove(c);
                tries = 1;
            }
        }
        if (saveGameButton/*Input.GetKeyDown(KeyCode.P)*/)
        {

            print("Remove -" + oresToRemove.Count + "- ores");
            List<SaveObject> temp = new List<SaveObject>();
            foreach (var item in saveRooms)
            {
                temp.Add(item);
            }
            saveRooms.Clear();

            foreach (var t in temp)
            {
                List<Ore> tempOre = new List<Ore>();
                foreach (var item in t.ores)
                {
                    tempOre.Add(item);
                    foreach (var o in oresToRemove)
                    {
                        if (item.x == o.x)
                        {
                            if (item.y == o.y)
                            {
                                //TODO: name
                                tempOre.Remove(item);
                            }
                        }
                    }

                }

                saveRooms.Add(new SaveObject
                {
                    x = t.x,
                    y = t.y,
                    seed = t.seed,
                    directions = t.directions,
                    ores = tempOre.ToArray(),
                });
            }

            Save();
            print("Saving!");
        }
    }

    private void GenerateMap()
    {
        map = new int[width, height];
        RandomFillMap();

        for (var i = 0; i < 4; i++)
        {
            SmoothMap();
        }

        ProcessMap();

        var borderSize = 5;
        var borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

        for (var x = 0; x < borderedMap.GetLength(0); x++)
        {
            for (var y = 0; y < borderedMap.GetLength(1); y++)
            {
                if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize)
                {
                    borderedMap[x, y] = map[x - borderSize, y - borderSize];
                }
                else
                {
                    borderedMap[x, y] = 1;
                }
            }
        }
    }

    private static int RandomNumber(int i)
    {
        var rand = Random.Range(0, i + 1);
        return rand;
    }

    private static List<Direction> ArrayListOfDirections()
    {
        var dir = new List<Direction> { Direction.Up, Direction.Down, Direction.Left, Direction.Right };
        return dir;
    }

    //TODO: bug fix! Somethimes this can delete a part of world. (It makes new connection)
    private void ConnectionToPrev()
    {
        if (tries == 0) return;

        var a = new Coord(coord.TileX + 1, coord.TileY);
        if (allMapCoords.Contains(a))
        {
            for (var i = width / 2; i < width; i++)
            {
                for (var j = height / 2 - pathThickness; j < height / 2 + 1 + pathThickness; j++)
                {
                    map[i, j] = 0;
                }
            }
        }

        var b = new Coord(coord.TileX - 1, coord.TileY);
        if (allMapCoords.Contains(b))
        {
            for (var i = 0; i < width / 2; i++)
            {
                for (var j = height / 2 - pathThickness; j < height / 2 + 1 + pathThickness; j++)
                {
                    map[i, j] = 0;
                }
            }
        }

        var c = new Coord(coord.TileX, coord.TileY + 1);
        if (allMapCoords.Contains(c))
        {
            for (var i = (width / 2) - pathThickness; i < (width / 2) + 1 + pathThickness; i++)
            {
                for (var j = height / 2; j < height; j++)
                {
                    map[i, j] = 0;
                }
            }
        }

        var d = new Coord(coord.TileX, coord.TileY - 1);
        if (allMapCoords.Contains(d))
        {
            for (var i = (width / 2) - pathThickness; i < (width / 2) + 1 + pathThickness; i++)
            {
                for (var j = 0; j < height / 2; j++)
                {
                    map[i, j] = 0;
                }
            }
        }
    }

    //TODO: this crates a path even if it doesn't connect to something!
    private void GenerateNextCoord()
    {
        switch (direction)
        {
            case Direction.Right:
                for (var i = width / 2; i < width; i++)
                {
                    for (var j = height / 2 - pathThickness; j < height / 2 + 1 + pathThickness; j++)
                    {
                        map[i, j] = 0;
                    }
                }

                var a = new Coord(coord.TileX + 1, coord.TileY);
                if (!allMapCoords.Contains(a))
                {
                    //queue.Add(a);
                    var empty = new GameObject("Null: " + a.TileX + ", " + a.TileY);
                    empty.transform.position = new Vector2((1 + a.TileX) * width - width / 2,
                        (1 + a.TileY) * height - height / 2);
                    empty.transform.parent = nullable.transform;
                    nodeList.Add(empty);
                }

                break;


            case Direction.Left:
                for (var i = 0; i < width / 2; i++)
                {
                    for (var j = height / 2 - pathThickness; j < height / 2 + 1 + pathThickness; j++)
                    {
                        map[i, j] = 0;
                    }
                }

                var b = new Coord(coord.TileX - 1, coord.TileY);
                if (!allMapCoords.Contains(b))
                {
                    //queue.Add(b);
                    var empty = new GameObject("Null: " + b.TileX + ", " + b.TileY);
                    empty.transform.position = new Vector2((1 + b.TileX) * width - width / 2,
                        (1 + b.TileY) * height - height / 2);
                    empty.transform.parent = nullable.transform;
                    nodeList.Add(empty);
                }

                break;


            case Direction.Up:
                for (var i = (width / 2) - pathThickness; i < (width / 2) + 1 + pathThickness; i++)
                {
                    for (var j = height / 2; j < height; j++)
                    {
                        map[i, j] = 0;
                    }
                }

                var c = new Coord(coord.TileX, coord.TileY + 1);
                if (!allMapCoords.Contains(c))
                {
                    //queue.Add(c);
                    var empty = new GameObject("Null: " + c.TileX + ", " + c.TileY);
                    empty.transform.position = new Vector2((1 + c.TileX) * width - width / 2,
                        (1 + c.TileY) * height - height / 2);
                    empty.transform.parent = nullable.transform;
                    nodeList.Add(empty);
                }

                break;


            case Direction.Down:
                for (var i = (width / 2) - pathThickness; i < (width / 2) + 1 + pathThickness; i++)
                {
                    for (var j = 0; j < height / 2; j++)
                    {
                        map[i, j] = 0;
                    }
                }

                var d = new Coord(coord.TileX, coord.TileY - 1);
                if (!allMapCoords.Contains(d))
                {
                    //queue.Add(d);
                    var empty = new GameObject("Null: " + d.TileX + ", " + d.TileY);
                    empty.transform.position = new Vector2((1 + d.TileX) * width - width / 2,
                        (1 + d.TileY) * height - height / 2);
                    empty.transform.parent = nullable.transform;
                    nodeList.Add(empty);
                }

                break;


            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.33f, 0.6f, 0.31f);
        Gizmos.DrawWireSphere(player.transform.position, radius);
    }


    #region CreatingAMap

    private void ProcessMap()
    {
        var wallRegions = GetRegions(1);

        foreach (var wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (var tile in wallRegion)
                {
                    map[tile.TileX, tile.TileY] = 0;
                }
            }
        }

        var roomRegions = GetRegions(0);
        var survivingRooms = new List<Room>();

        foreach (var roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (var tile in roomRegion)
                {
                    map[tile.TileX, tile.TileY] = 1;
                }
            }
            else
            {
                survivingRooms.Add(new Room(roomRegion, map));
            }
        }

        survivingRooms.Sort();
        survivingRooms[0].IsMainRoom = true;
        survivingRooms[0].IsAccessibleFromMainRoom = true;

        ConnectClosestRooms(survivingRooms, true);
    }

    private void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
    {
        var roomListA = new List<Room>();
        var roomListB = new List<Room>();

        if (forceAccessibilityFromMainRoom)
        {
            foreach (var room in allRooms)
            {
                if (room.IsAccessibleFromMainRoom)
                {
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        var bestDistance = 0;
        var bestTileA = new Coord();
        var bestTileB = new Coord();
        var bestRoomA = new Room();
        var bestRoomB = new Room();
        var possibleConnectionFound = false;

        foreach (var roomA in roomListA)
        {
            if (!forceAccessibilityFromMainRoom)
            {
                possibleConnectionFound = false;
                if (roomA.ConnectedRooms.Count > 0)
                {
                    continue;
                }
            }

            foreach (var roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB))
                {
                    continue;
                }

                foreach (var t in roomA.EdgeTiles)
                {
                    foreach (var t1 in roomB.EdgeTiles)
                    {
                        var tileA = t;
                        var tileB = t1;
                        var distanceBetweenRooms =
                            (int)(Mathf.Pow(tileA.TileX - tileB.TileX, 2) + Mathf.Pow(tileA.TileY - tileB.TileY, 2));

                        if (distanceBetweenRooms >= bestDistance && possibleConnectionFound) continue;
                        bestDistance = distanceBetweenRooms;
                        possibleConnectionFound = true;
                        bestTileA = tileA;
                        bestTileB = tileB;
                        bestRoomA = roomA;
                        bestRoomB = roomB;
                    }
                }
            }

            if (possibleConnectionFound && !forceAccessibilityFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (possibleConnectionFound && forceAccessibilityFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(allRooms, true);
        }

        if (!forceAccessibilityFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    private void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB)
    {
        Room.ConnectRooms(roomA, roomB);
        //Debug.DrawLine(CoordToWorldPoint(tileA), CoordToWorldPoint(tileB), Color.red, 100);

        var line = GetLine(tileA, tileB);
        foreach (var c in line)
        {
            DrawCircle(c, passageTunnel);
        }
    }

    private void DrawCircle(Coord c, int r)
    {
        for (var x = -r; x <= r; x++)
        {
            for (var y = -r; y <= r; y++)
            {
                if (x * x + y * y <= r * r)
                {
                    var drawX = c.TileX + x;
                    var drawY = c.TileY + y;

                    if (IsInMapRange(drawX, drawY))
                    {
                        map[drawX, drawY] = 0;
                    }
                }
            }
        }
    }

    private static IEnumerable<Coord> GetLine(Coord from, Coord to)
    {
        var line = new List<Coord>();

        var x = from.TileX;
        var y = from.TileY;

        var dx = to.TileX - from.TileX;
        var dy = to.TileY - from.TileY;

        var inverted = false;
        var step = Math.Sign(dx);
        var gradientStep = Math.Sign(dy);

        var longest = Mathf.Abs(dx);
        var shortest = Mathf.Abs(dy);

        if (longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        var gradientAccumulation = longest; // / 2;
        for (var i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));

            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }

                gradientAccumulation -= longest;
            }
        }

        return line;
    }

    private Vector3 CoordToWorldPoint(Coord tile)
    {
        //return new Vector3(-width * numOfGensRight/ 2 + .5f + tile.tileX, -height / 2 + .5f + tile.tileY, 2);
        return new Vector3(.5f + tile.TileX, .5f + tile.TileY, 0);
    }

    private List<List<Coord>> GetRegions(int tileType)
    {
        var regions = new List<List<Coord>>();
        var mapFlags = new int[width, height];

        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    var newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (var tile in newRegion)
                    {
                        mapFlags[tile.TileX, tile.TileY] = 1;
                    }
                }
            }
        }

        return regions;
    }

    private List<Coord> GetRegionTiles(int startX, int startY)
    {
        var tiles = new List<Coord>();
        var mapFlags = new int[width, height];
        var tileType = map[startX, startY];

        var queueTiles = new Queue<Coord>();
        queueTiles.Enqueue(new Coord(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queueTiles.Count > 0)
        {
            var tile = queueTiles.Dequeue();
            tiles.Add(tile);

            for (var x = tile.TileX - 1; x <= tile.TileX + 1; x++)
            {
                for (var y = tile.TileY - 1; y <= tile.TileY + 1; y++)
                {
                    // if is 0-width, 0-height and
                    if (IsInMapRange(x, y) && (y == tile.TileY || x == tile.TileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queueTiles.Enqueue(new Coord(x, y));
                        }
                    }
                }
            }
        }

        return tiles;
    }

    private bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    // Creating a map!
    private void RandomFillMap()
    {
        if (useRandomSeed)
        {
            //seed = Time.time.ToString(CultureInfo.CurrentCulture);
            seed = RandomNumber(10000000).ToString(CultureInfo.CurrentCulture);
        }

        var pseudoRandom = new System.Random(seed.GetHashCode());

        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                // Assigning walls
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                // Assigning empty spaces!
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    private void SmoothMap()
    {
        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                var neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    map[x, y] = 1;
                else if (neighbourWallTiles < 4)
                    map[x, y] = 0;
            }
        }
    }

    private int GetSurroundingWallCount(int gridX, int gridY)
    {
        var wallCount = 0;
        for (var neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (var neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                // if is in map range (from 0 to width and height)
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    private struct Coord
    {
        public readonly int TileX;
        public readonly int TileY;

        public Coord(int x, int y)
        {
            TileX = x;
            TileY = y;
        }
    }


    private class Room : IComparable<Room>
    {
        public readonly List<Room> ConnectedRooms;
        public readonly List<Coord> EdgeTiles;
        private readonly int roomSize;
        public bool IsAccessibleFromMainRoom;

        public Room()
        {
        }

        public Room(List<Coord> roomTiles, int[,] map)
        {
            var tiles = roomTiles;
            roomSize = tiles.Count;
            ConnectedRooms = new List<Room>();

            EdgeTiles = new List<Coord>();
            foreach (var tile in tiles)
            {
                for (var x = tile.TileX - 1; x <= tile.TileX + 1; x++)
                {
                    for (var y = tile.TileY - 1; y <= tile.TileY + 1; y++)
                    {
                        if (x != tile.TileX && y != tile.TileY) continue;
                        if (map[x, y] == 1)
                        {
                            EdgeTiles.Add(tile);
                        }
                        else if (map[x, y] != 0)
                        {
                            Debug.LogError("No tiles to be placed!");
                        }
                    }
                }
            }
        }

        public bool IsMainRoom { get; set; }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }

        private void SetAccessibleFromMainRoom()
        {
            if (IsAccessibleFromMainRoom) return;
            IsAccessibleFromMainRoom = true;
            foreach (var connectedRoom in ConnectedRooms)
            {
                connectedRoom.SetAccessibleFromMainRoom();
            }
        }

        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if (roomA.IsAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.IsAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }

            roomA.ConnectedRooms.Add(roomB);
            roomB.ConnectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return ConnectedRooms.Contains(otherRoom);
        }
    }

    #endregion

}
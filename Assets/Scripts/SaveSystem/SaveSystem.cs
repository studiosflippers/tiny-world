﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SaveSystem
{

    private static readonly string SAVE_FOLDER = Application.dataPath + "/saves/";
    private static readonly string SAVE_EXTENSION =".json";

    public static void Init()
    {
        if (Directory.Exists(SAVE_FOLDER)) return;
        Directory.CreateDirectory(SAVE_FOLDER);
    }

    public static void Save(string fileName, string saveString, bool overwrite)
    {
        int saveNum = 1;
        string saveFileName = fileName + saveNum;
        if (!overwrite)
        {
            while (File.Exists(SAVE_FOLDER + saveFileName + SAVE_EXTENSION))
            {
                saveNum++;
                saveFileName = fileName + saveNum;
            }
        }
        File.WriteAllText(SAVE_FOLDER + saveFileName + SAVE_EXTENSION, saveString);
    }

    public static string Load(string fileName)
    {
        if (File.Exists(SAVE_FOLDER + fileName + SAVE_EXTENSION))
        {
            string saveString = File.ReadAllText(SAVE_FOLDER + fileName + SAVE_EXTENSION);
            return saveString;
        }
        else
        {
            return null;
        }
    }

    public static string LoadRecent()
    {
        DirectoryInfo dirInfo = new DirectoryInfo(SAVE_FOLDER);
        FileInfo[] saveFiles = dirInfo.GetFiles("*.json");
        FileInfo mostRecentFile = null;
        foreach (var fileInfo in saveFiles)
        {
            if (mostRecentFile == null)
            {
                mostRecentFile = fileInfo;
            }

            else if (fileInfo.LastWriteTime > mostRecentFile.LastWriteTime)
            {
                mostRecentFile = fileInfo;
            }
        }

        if (mostRecentFile != null)
        {
            string saveString = File.ReadAllText(mostRecentFile.FullName);
            return saveString;
        }
        else
        {
            return null;
        }
    }

    public static void SaveObject(object saveObject)
    {
        SaveObject("save", saveObject, false);
    }

    public static void SaveObject(object saveObject, bool overwrite)
    {
        SaveObject("save", saveObject, overwrite);
    }

    public static void SaveObject(string fileName, object saveObject, bool overwrite)
    {
        string json = JsonUtility.ToJson(saveObject);
        Save(fileName, json, overwrite);
    }

    public static TSaveObject LoadRecentObject<TSaveObject>()
    {
        string saveString = LoadRecent();
        if (saveString != null)
        {
            TSaveObject saveObject = JsonUtility.FromJson<TSaveObject>(saveString);
            return saveObject;
        }
        else
        {
            return default(TSaveObject);
        }
    }

    public static TSaveObject LoadObject<TSaveObject>(string fileName)
    {
        string saveString = Load(fileName);
        if (saveString != null)
        {
            TSaveObject saveObject = JsonUtility.FromJson<TSaveObject>(saveString);
            return saveObject;
        }
        else
        {
            return default(TSaveObject);
        }
    }
}

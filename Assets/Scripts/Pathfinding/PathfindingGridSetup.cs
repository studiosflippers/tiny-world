﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingGridSetup : MonoBehaviour
{
    public static PathfindingGridSetup Instance { private set; get; }

    public Grid<Node> pathfindingGrid;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        pathfindingGrid = new Grid<Node>(30, 15, 1f, Vector3.zero, (Grid<Node> grid, int x, int y) => new Node(grid, x, y));
        pathfindingGrid.GetValue(2, 0).SetIsWalkable(false);
        pathfindingGrid.GetValue(2, 1).SetIsWalkable(false);
        pathfindingGrid.GetValue(2, 2).SetIsWalkable(false);
        pathfindingGrid.GetValue(3, 3).SetIsWalkable(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

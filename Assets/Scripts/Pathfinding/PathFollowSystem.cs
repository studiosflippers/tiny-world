﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class PathFollowSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((DynamicBuffer<PathPosition> pathPositionBuffer, ref Translation translation, ref PathFollow pathFollow) =>
        {
            if (pathFollow.pathIndex >= 0)
            {
                int2 pathPos = pathPositionBuffer[pathFollow.pathIndex].position;

                float3 targetPos = new float3(pathPos.x, pathPos.y, 0);
                float3 moveDir = math.normalizesafe(targetPos - translation.Value);
                float moveSpeed = 3f;

                translation.Value += moveDir * moveSpeed * Time.DeltaTime;

                if (math.distance(translation.Value, targetPos) < 0.1f)
                {
                    //Next waypoint
                    pathFollow.pathIndex--;
                }
            }
        });
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class UnitMoveOrderSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        //if (PlayerController.Instance.leftMouse)
        //{
        //    Entities.ForEach((Entity e, ref Translation t) =>
        //    {
        //        EntityManager.AddComponentData(e, new PathfindingParams
        //        {
        //            startPos = new Unity.Mathematics.int2(0, 0),
        //            endPos = new Unity.Mathematics.int2(20, 0)
        //        });
        //    });
        //    PlayerController.Instance.leftMouse = false;
        //}

        if (PlayerController.Instance.leftMouse)
        {
            var v = PlayerController.Instance.mouse;
            var c = Camera.main;
            var mousePosition = c.ScreenToWorldPoint(new Vector3(v.x, v.y, 5));
            float cellSize = PathfindingGridSetup.Instance.pathfindingGrid.GetCellSize();

            PathfindingGridSetup.Instance.pathfindingGrid.GetAxies(mousePosition + new Vector3(1, 1) * cellSize * +.5f, out int endX, out int endY);

            ValidateGridPosition(ref endX, ref endY);

            Entities.ForEach((Entity e, DynamicBuffer<PathPosition> PathPosition, ref Translation t) =>
            {
                PathfindingGridSetup.Instance.pathfindingGrid.GetAxies(t.Value + new float3(1, 1, 0) * cellSize * +.5f, out int startX, out int startY);

                ValidateGridPosition(ref startX, ref startY);
                EntityManager.AddComponentData(e, new PathfindingParams
                {
                    startPos = new int2(startX, startY),
                    endPos = new int2(endX, endY)
                });
            });
        }
    }

    private void ValidateGridPosition(ref int x, ref int y)
    {
        x = math.clamp(x, 0, PathfindingGridSetup.Instance.pathfindingGrid.GetWidth() - 1);
        y = math.clamp(y, 0, PathfindingGridSetup.Instance.pathfindingGrid.GetHeight() - 1);
    }
}

﻿using Unity.Entities;

namespace _Scripts
{
    public struct LevelComponent : IComponentData
    {
        public float level;
    }
}

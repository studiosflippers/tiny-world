﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    public Sprite normalCursor;
    public Sprite litCursor;
    //public Sprite bowCursor;
    public GameObject player;
    public int range;

    SpriteRenderer spriteRenderer;

    private Resources instance;
    // Start is called before the first frame update
    void Start()
    {
        instance = Resources.Instance;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(this.transform.position, player.transform.position) > range)
        {
            spriteRenderer.sprite = litCursor;
        }
        else
        {
            spriteRenderer.sprite = litCursor;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class HomeScript : MonoBehaviour
{
    GameObject player;
    public Vector2 entry;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = entry;
    }

    // Update is called once per frame
    void Update()
    {
    }
}

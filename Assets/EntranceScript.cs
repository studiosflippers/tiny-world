﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntranceScript : MonoBehaviour
{
    PlayerController pc;

    public Object houseScene;
    public Object mainScene;
    public GameObject interactableSprite;
    Collider2D entrance;
    GameObject player;

    public List<GameObject> gameObjectsToDisable = new List<GameObject>();
    string i;
    

    public Vector2 pos;
    bool interacting;


    // Start is called before the first frame update
    void Start()
    {
        interactableSprite.SetActive(false);
        pc = PlayerController.Instance;
        i = houseScene.name;
        entrance = GetComponent<CircleCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        interacting = pc.interacting;
        if (entrance.IsTouching(player.GetComponentInChildren<Collider2D>()))
        {
            interactableSprite.SetActive(true);
            if (interacting)
            {
                foreach (var item in gameObjectsToDisable)
                {
                    item.SetActive(false);
                }
                pos = player.transform.position;
                SceneManager.LoadSceneAsync(i);
            }
        }
        else
        {
            interactableSprite.SetActive(false);
        }
    }
}
